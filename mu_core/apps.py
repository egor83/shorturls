from django.apps import AppConfig


class MuCoreConfig(AppConfig):
    name = 'mu_core'
