from rest_framework import permissions


class IsOwnerAdminOrPublic(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        """
        Allow access if current user is an owner or admin,
        or if the ShortUrl is public.
        """
        if (obj.owner == request.user) or request.user.is_superuser:
            return True
        if not obj.is_private:
            return True
        return False
