
from django.contrib import admin

from .models import ShortUrl


class ShortUrlAdmin(admin.ModelAdmin):
    list_display = ('id', 'long_url', 'short_url', 'is_private', 'owner')

admin.site.register(ShortUrl, ShortUrlAdmin)
