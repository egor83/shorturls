
from django.contrib.auth.models import User

from rest_framework import serializers

from mu_core.models import ShortUrl


class ShortUrlSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(view_name='user-detail', required=False, read_only=True)
    short_url = serializers.ReadOnlyField()

    #def validate(self, data):
        #if ('is_private' in data ) and ('owner' not in data):
            #raise serializers.ValidationError("Private short URLs need to have owner set")
        #return data

    class Meta:
        model = ShortUrl
        fields = ['url', 'id', 'long_url', 'short_url', 'owner', 'is_private']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    shorturl_set = serializers.HyperlinkedRelatedField(many=True, queryset=ShortUrl.objects.all(), view_name='shorturl-detail')

    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'shorturl_set']
