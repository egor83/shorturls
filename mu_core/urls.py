
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from mu_core import views


urlpatterns = [
    path('', views.api_root),

    # handling short URLs
    path('list_all', views.ShortUrlListAll.as_view(), name='short-url-list-all'),
    path('short_urls', views.ShortUrlList.as_view(), name='short-url-list'),
    path('short_urls/<int:pk>', views.ShortUrlDetail.as_view(), name='shorturl-detail'),
    path('add_short_url', views.NewShortUrl.as_view(), name='add-short-url'),

    # handling users
    path('users', views.UserList.as_view(), name='user-list'),
    path('users/<int:pk>', views.UserDetail.as_view(), name='user-detail'),
    path('add_user', views.add_new_user, name='add-user'),

    # redirecting short URLs
    path('<slug>', views.RedirectShortUrl.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
