
import logging
from collections import OrderedDict

from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import redirect, render

from rest_framework import generics, permissions, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAdminUser
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from mu_core.models import ShortUrl
from mu_core.permissions import IsOwnerAdminOrPublic
from mu_core.serializers import ShortUrlSerializer, UserSerializer


logger = logging.getLogger(__name__)


@api_view(['GET'])
def api_root(request, format=None):
    menu = OrderedDict()
    # registration if logged out
    if not request.user.is_authenticated:
        menu['Register a new user'] = reverse('add-user', request=request, format=format)
    menu['Add short URL'] = reverse('add-short-url', request=request, format=format)
    menu['List short URLs'] = reverse('short-url-list', request=request, format=format)
    # only show users, list_all to admins
    if request.user.is_superuser:
        menu['List all short URLs'] = reverse('short-url-list-all', request=request, format=format)
        menu['List users'] = reverse('user-list', request=request, format=format)
    return Response(menu)


class ShortUrlListAll(generics.ListAPIView):
    """List all ShortURLs."""
    queryset = ShortUrl.objects.all()
    serializer_class = ShortUrlSerializer
    permission_classes = [IsAdminUser,]

    # TODO left out creating a new one, see DRF tut 3 - do I need it though?
    # NB handle generating short_url - custom save? As w/ highlighting in tutorial


class ShortUrlList(generics.ListAPIView):
    """List all public SUs, AND this user's SUs."""
    serializer_class = ShortUrlSerializer

    def get_queryset(self):
        return ShortUrl.objects.filter(Q(is_private=False) | Q(owner=self.request.user.pk))


class ShortUrlDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ShortUrl.objects.all()
    serializer_class = ShortUrlSerializer
    permission_classes = [IsOwnerAdminOrPublic,]


class NewShortUrl(APIView):
    """Create a new short URL."""
    renderer_classes = [JSONRenderer, TemplateHTMLRenderer,]
    template_name = 'mu_core/new_shorturl_form.html'

    def get(self, request, format=None):
        serializer = ShortUrlSerializer(context={'request': request})
        if format == 'json':
            return Response(serializer.data)
        else:
            return Response({'serializer': serializer})

    def post(self, request, format=None):
        is_created, is_valid, ser = self.generate_short_url_view(request, format)
        if is_valid:
            messages.success(request, 'Short URL for the given long URL:')
            messages.success(request, ser.data['short_url'])
        else:
            messages.error(request, ser.errors)
        return Response({'serializer': ser})

    def put(self, request, format=None):
        is_created, is_valid, ser = self.generate_short_url_view(request, format)
        if is_valid:
            if is_created:
                return Response(ser.data, status=status.HTTP_201_CREATED)
            else:
                return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def generate_short_url_view(self, request, format):
        """Generate short URL. Return: (is_created, is_valid, serializer)"""

        data = request.data

        # search for a given long URL in public short URLs and this user's short URLs;
        # if none exist, create a new one
        # TODO handle forcing new short_url for existing long_url
        long_url = data['long_url']
        is_private = data.get('is_private', False)
        # only search private SUs if the new SU is private too (to enable creating new public SU when you already have private)
        if is_private:
            sus = ShortUrl.objects.filter(Q(is_private=False) | Q(owner=self.request.user.pk))
        else:
            # new SU is public - do not search private SUs
            sus = ShortUrl.objects.filter(Q(is_private=False))
        sus = sus.filter(long_url=long_url)

        if sus.count() == 0:
            logger.info('creating a new ShortUrl for {}'.format(long_url))
            short_url = ShortUrl.generate_short_url(long_url, request)
            if is_private:
                owner = request.user
            else:
                owner = None

            ser = ShortUrlSerializer(data=data, context={'request': request})
            if ser.is_valid():
                ser.save(short_url=short_url, owner=owner)
                return (True, True, ser)
            else:
                logger.error(ser.errors)
                return (True, False, ser)
        else:
            # TODO handle (forced) multiple long_urls here
            logger.info('returning an existing ShortUrl')
            su = sus[0]
            ser = ShortUrlSerializer(su, context={'request': request})
            #logger.debug("data: {}\n".format(data))
            return (False, True, ser)


class RedirectShortUrl(APIView):
    permission_classes = [IsOwnerAdminOrPublic,]

    def get(self, request, slug, format=None):
        return self.redirect_su(request, slug, format)

    def post(self, request, slug, format=None):
        return self.redirect_su(request, slug, format)

    def redirect_su(self, request, slug, format=None):
        try:
            short_url = request.build_absolute_uri(slug)
            su = ShortUrl.objects.get(short_url=short_url)
            self.check_object_permissions(request, su)
            return redirect(su.long_url)
        except ShortUrl.DoesNotExist:
            logger.error('Short URL {} not found'.format(short_url))
            return Response(status=status.HTTP_404_NOT_FOUND)


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminUser,]


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminUser,]


def add_new_user(request):
    # TODO do I need to handle JSON calls here?
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()

    return render(request, 'mu_core/register.html', {'form': form})
