
from django.db import models

import hashlib
import logging
from datetime import datetime


MIN_HASH_LEN = 3

logger = logging.getLogger(__name__)


class ShortUrl(models.Model):
    long_url = models.URLField()
    short_url = models.CharField(max_length=200, unique=True)
    owner = models.ForeignKey('auth.User', default=None, blank=True, null=True, on_delete=models.CASCADE)
    is_private = models.BooleanField(default=False)
    # TODO: creation datetime?
    # references to tags
    # screenshot
    # archive.is link

    def __str__(self):
        return "{} (ID {})".format(self.long_url, self.id,)

    def generate_short_url(long_url, request):
        """For now, md5sum of self.long_url + dttm string. Returns empty string on error."""

        logger.info('Long URL: {}'.format(long_url))
        dttm = datetime.utcnow().strftime('%Y/%m/%d %H:%M:%S:%f')
        hash_input = "{} {}".format(long_url, dttm)
        logger.info('Hash input: {}'.format(hash_input))
        url_hs = hashlib.md5(hash_input.encode("utf-8")).hexdigest()
        logger.info('Full hash: {}'.format(url_hs))
        max_len = len(url_hs)

        # make sure there's no collisions; if there is, increase short URL length till it's unique
        curr_len = MIN_HASH_LEN
        curr_shorturl = request.build_absolute_uri(url_hs[:curr_len])
        while ShortUrl.objects.filter(short_url=curr_shorturl).exists():
            logger.warn('Collision on {}: hash {}'.format(hash_input, curr_shorturl))
            curr_len += 1
            curr_shorturl = request.build_absolute_uri(url_hs[:curr_len])
            logger.info('Longer hash: {}'.format(curr_shorturl))
            if curr_len == max_len and ShortUrl.objects.filter(short_url=curr_shorturl).exists():
                logger.error('Full hash collision; long_url: {}; short_url/hash: {}'.format(long_url, url_hs))
                return ''

        logger.info('Short URL: {}\n'.format(curr_shorturl))
        return curr_shorturl
